package config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigPropertyReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigPropertyReader.class.getName());
    private static ConfigPropertyReader configdata;

    private String url;
    private String browser;
    private String userPassword;
    private String user1;

    /*
    Intiates the constructor with values read from propertyfile
     */
    private ConfigPropertyReader() {
        this.url = readPropertiesFile().getProperty("url");
        this.browser = readPropertiesFile().getProperty("browser");
        this.userPassword = readPropertiesFile().getProperty("userPassword");
        this.user1 = readPropertiesFile().getProperty("user1");
    }

    public static ConfigPropertyReader getInstance() {
        if (configdata==null) {
            configdata = new ConfigPropertyReader();
        }
        return configdata;
    }

    public String getUrl() {
        return url;
    }
    public String getBrowser() {
        return browser;
    }
    public String getUserPassword() {
        return userPassword;
    }
    public String getUser1() {
        return user1;
    }

    public Properties readPropertiesFile() {
        FileInputStream fileInputStream;
        Properties prop = null;
        try {
            String filepath = System.getProperty("user.dir") + "/src/test/resources/configs/config.properties";
            File fileName = new File(filepath);
            fileInputStream = new FileInputStream(fileName);
            prop = new Properties();
            prop.load(fileInputStream);
            LOGGER.info("Successfully read property file");
        } catch (IOException ioException) {
            LOGGER.error("Error while reading properties file", ioException);
        }
        return prop;
    }
}
