package utils;

import config.ConfigPropertyReader;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Base {

    private static WebDriver driver;
    public static final String CHROME = "chrome";
    public static final String FIREFOX = "firefox";
    private static WebDriverWait webDriverWait;
    public static final long WAIT_FOR_FIVE_SECOND = 5;
    public static final long WAIT_FOR_TEN_SECOND = 10;

    public Base() {
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public WebDriver initializeDriver() {
        String browser = ConfigPropertyReader.getInstance().getBrowser();
        if (browser.equals(CHROME)) {
            driver = WebDriverManager.chromedriver().create(); //Using the new WebDriver 5x version which replaces  the below code
            driver.manage().window().maximize();
//            WebDriverManager.chromedriver().setup();
//            driver=new ChromeDriver();
        }
        else if (browser.equals(FIREFOX)) {
            driver = WebDriverManager.firefoxdriver().create(); //Using the new WebDriver 5x version which minimizes the code
        }
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(WAIT_FOR_FIVE_SECOND));
        return driver;
    }

    public void finishDriver() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }

    /*
    Implementing an example for Overloading methods for implicitWait()
     */
    public void implicitWait() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(WAIT_FOR_FIVE_SECOND));
    }
    public void implicitWait(long time) {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(time));
    }

    public static void waitUntilElementPresentByXpath(By locator) {
        webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(WAIT_FOR_FIVE_SECOND));
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
    public static void waitUntilElementIsVisible(By locator) {
        webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(WAIT_FOR_FIVE_SECOND));
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
    public static void waitUntilElementIsInvisible(By locator) {
        webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(WAIT_FOR_FIVE_SECOND));
        webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }
    public static void waitUntilElementIsClickable(By locator) {
        webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(WAIT_FOR_FIVE_SECOND));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    public static void sleep(int time){
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void pageLoad() {
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(WAIT_FOR_FIVE_SECOND));
    }
    public void pageLoad(long time) {
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(time));
    }

    public void deleteAllCookies() {
        driver.manage().deleteAllCookies();
    }
    public void findElementAndClick(By element) {
        driver.findElement(element).click();
    }

}
