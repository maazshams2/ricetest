package stepDefinitions;

import utils.Base;
import pages.HomePage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HomePageSteps {
    private static final Logger LOGGER = LoggerFactory.getLogger(HomePageSteps.class.getName());

    //Initializing homePage object with driver from Base class
    HomePage home = new HomePage(Base.getDriver());

    @Given("User navigates to the homepage")
    public void user_navigates_to_the_homepage() throws InterruptedException {
        LOGGER.info("Navigating to saucedemo.com");
        home.navigateToCoinMarketCap();
        home.closePopUps();
    }

    @Then("User verifies {int} rows are displayed")
    public void userVerifiesRowsAreDisplayed(int rows) {
        home.validateRowCount(rows);
    }

    @Then("User clicks on Filters button")
    public void userClicksOnFiltersButton() throws InterruptedException {
        home.clickOnFilterbutton();
    }

    @When("User filter records by MarketCap {string} and Price {string}")
    public void userFilterRecordsByMarketCapAndPrice(String marketCap, String price) throws InterruptedException {
        home.clickMarketCapPriceAndAddValues(marketCap, price);
        home.clickShowResults();
    }
}
