package stepDefinitions;

import utils.Base;
import config.ConfigPropertyReader;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Hook extends Base{

    private static final Logger LOGGER = LoggerFactory.getLogger(Hook.class.getName());

    @Before
    public void setUp() {
        try{
            ConfigPropertyReader.getInstance();
            LOGGER.info("Opening browser");
            initializeDriver();
            pageLoad(10);
            deleteAllCookies();
        }
        catch (Exception e){
            LOGGER.error("Error while opening browser", e.getCause());
        }
    }

    @After
    public void endDriver() {
        finishDriver();
    }
}
