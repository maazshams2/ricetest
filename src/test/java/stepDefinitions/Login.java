package stepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.Page;

import java.awt.*;

public class Login extends Page {
    private static final Logger LOGGER = LoggerFactory.getLogger(HomePageSteps.class.getName());

    public Login() throws AWTException {
    }

    @Given("User navigates to the sauce demo website")
    public void userNavigatesToTheSauceDemoWebsite() {
        LOGGER.info("Navigating to saucedemo.com");
        getLoginPage().navigateToSauceDemo();
    }

    @When("User enters correct credentials")
    public void userEntersCorrectCredentials() throws InterruptedException {
        LOGGER.info("Entering correct username and password");
        getLoginPage().enterCredentials();
    }

    @And("User clicks on login")
    public void userClicksOnLogin() {
        LOGGER.info("User clicks on login");
        getLoginPage().clickLoginButton();
    }

    @And("User logins successfully")
    public void userLoginsSuccessfully() {
        LOGGER.info("User logins successfully");
        getLoginPage().enterCredentials();
        getLoginPage().clickLoginButton();
    }

}
