package stepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.Page;

import java.awt.*;

public class Cart extends Page {
    private static final Logger LOGGER = LoggerFactory.getLogger(HomePageSteps.class.getName());

    public Cart() throws AWTException {
    }

    @And("Cart has all items added to it")
    public void cartHasAllItemsAddedToIt() {
        LOGGER.info("User has all items added to it");
        Assert.assertEquals(getCartPage().getCartItemsCount(), getInventoryPage().getCartItems());
    }
}
