package stepDefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.Page;

import java.awt.*;
import java.util.Random;

public class Inventory extends Page {
    private static final Logger LOGGER = LoggerFactory.getLogger(HomePageSteps.class.getName());
    private Random rand = new Random();

    public Inventory() throws AWTException {
    }

    @Then("User is successfully logged in")
    public void userIsSuccessfullyLoggedIn() {
        getInventoryPage().validateInventoryPage();
    }

    @When("User adds {int} random items to cart")
    public void userAddsRandomItemsToCart(int items) {
        LOGGER.info("User add " + items + " random items to cart");
        for (int i=0 ; i<items ; i++){
            int itemNumber = rand.nextInt(getInventoryPage().getInventoryItemsCount());
            getInventoryPage().clickAddToCart(itemNumber);
        }
    }

    @Then("User clicks on cart icon")
    public void userClicksOnCartIcon() {
        getInventoryPage().clickShoppingCart();
        getCartPage().validateCartTitle();
    }
}
