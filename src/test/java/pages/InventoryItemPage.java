package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static utils.Base.waitUntilElementIsClickable;
import static utils.Base.waitUntilElementIsVisible;

public class InventoryItemPage {
    private WebDriver driver;
    public InventoryItemPage(WebDriver driver) {
        this.driver = driver;
    }

    private By byBackToProductsButton = By.id("back-to-products");
    private By byInventoryDetailsName = By.className("inventory_details_name");
    private By byInventoryDetailsDesc = By.className("inventory_details_desc");
    private By byInventoryDetailsPrice = By.className("inventory_details_price");

    public void validateInventoryDetails(String name, String description, String price) {
        waitUntilElementIsVisible(byInventoryDetailsName);
        Assert.assertTrue(driver.findElement(byInventoryDetailsName).getText().equalsIgnoreCase(name));
        Assert.assertTrue(driver.findElement(byInventoryDetailsDesc).getText().equalsIgnoreCase(description));
        Assert.assertTrue(driver.findElement(byInventoryDetailsPrice).getText().equalsIgnoreCase(price));
    }

    public void goBackToProducts(){
        waitUntilElementIsClickable(byBackToProductsButton);
        driver.findElement(byBackToProductsButton).click();
    }
}
