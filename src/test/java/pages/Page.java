package pages;

import utils.Base;

import java.awt.*;

public class Page {
    public Page() throws AWTException {
    }

    private final CartPage cartPage = new CartPage(Base.getDriver());
    private final CheckoutInformationPage checkoutInformationPage = new CheckoutInformationPage(Base.getDriver());
    private final CheckoutOverviewPage checkoutOverviewPage = new CheckoutOverviewPage(Base.getDriver());
    private final InventoryItemPage inventoryItemPage = new InventoryItemPage(Base.getDriver());
    private final InventoryPage inventoryPage = new InventoryPage(Base.getDriver());
    private final LoginPage loginPage = new LoginPage(Base.getDriver());

    protected CartPage getCartPage() {return cartPage;}
    protected CheckoutInformationPage getCheckoutInformationPage() {return checkoutInformationPage;}
    protected CheckoutOverviewPage getCheckoutOverviewPage() {return checkoutOverviewPage;}
    protected InventoryItemPage getInventoryItemPage() {return inventoryItemPage;}
    protected InventoryPage getInventoryPage() {return inventoryPage;}
    protected LoginPage getLoginPage() {return loginPage;}

}
