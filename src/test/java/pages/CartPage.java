package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage {
    private WebDriver driver;
    public CartPage(WebDriver driver) {
        this.driver = driver;
    }

    private By byTitle = By.className("title");
    private By byCartItems = By.className("cart_item");
    private By byCartRemoveButtom = By.className("cart_button");
    private By byCheckoutButton = By.id("checkout");
    private By byContinueShoppingButton = By.id("continue-shopping");

    public void validateCartTitle(){
        Assert.assertTrue(driver.findElement(byTitle).getText().equalsIgnoreCase( "your cart"));
    }
    public int getCartItemsCount(){
        return driver.findElements(byCartItems).size();
    }
    public void clickCheckoutButton(){
        driver.findElement(byCheckoutButton).click();
    }
    public void clickContinueShoppingButton(){
        driver.findElement(byContinueShoppingButton).click();
    }
}
