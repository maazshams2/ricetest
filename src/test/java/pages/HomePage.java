package pages;

import utils.Base;
import config.ConfigPropertyReader;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class HomePage extends Base {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomePage.class.getName());
    private WebDriver driver;

    By cookie_button_close = By.className("cmc-cookie-policy-banner__close");
    By list_of_rows = By.cssSelector(".cmc-table > tbody > tr");
    By page_popUp = By.cssSelector("body > div.sc-8ukhc-0.ljgYka");
    By filter_button = By.cssSelector(".scroll-initial > div:nth-child(3) > div:nth-child(2) > button.table-control-filter");
    By add_filter_button = By.xpath("//button[normalize-space()='+ Add Filter']");
    By market_Cap_button = By.xpath("//button[normalize-space()='Market Cap']");
    By applyFilter_button = By.xpath("//button[normalize-space()='Apply Filter']");
    By price_button = By.xpath("//button[normalize-space()='Price']");
    By show_results_button = By.xpath("//button[normalize-space()='Show results']");
    By footer_advert = By.xpath("//button[normalize-space()='Maybe later']");
    By table = By.cssSelector(".cmc-table > tbody > tr");

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void navigateToCoinMarketCap() {
        driver.get(ConfigPropertyReader.getInstance().getUrl());
    }

    public void closePopUps() throws InterruptedException {
        LOGGER.info("Closing 3 popUps-banners");
        findElementAndClick(page_popUp);
        findElementAndClick(cookie_button_close);
        /*
        Explicit wait for some reason was not working, due to shortage of time
        I am going with a bad practice i.e Thread.Sleep()
         */
        Thread.sleep(20000);
        //here test might fail due to improper Element handling. Please rerun
        findElementAndClick(footer_advert);
        LOGGER.info("Closed 3 popUps-banners");
    }

    /*
    Asserting that I am able to see 100 rows as expected
     */
    public void validateRowCount(int count) {
        List<WebElement> list = driver.findElements(list_of_rows);
        Assert.assertEquals(list.size(), count);
        LOGGER.info("Element found: " + list_of_rows);
    }

    public void clickOnFilterbutton() {
        LOGGER.info("Clicking filters and Add Filter buttons");
        findElementAndClick(filter_button);
        findElementAndClick(add_filter_button);
        LOGGER.info("Clicked filters and Add Filter buttons");
    }

    /*
    Adding some Thread.Sleep() due to lack of time for correctly implenting awaits.
     */
    public void clickMarketCapPriceAndAddValues(String marketCap, String price) throws InterruptedException {
        LOGGER.info("Clicking Market Cap and add Value");
        findElementAndClick(market_Cap_button);
        driver.findElement(By.xpath("//button[normalize-space()='" + marketCap + "']")).click();
        findElementAndClick(applyFilter_button);
        Thread.sleep(2000);
        LOGGER.info("Clicked Market Cap and added Value");

        LOGGER.info("Clicking Price and add Value");
        findElementAndClick(price_button);
        driver.findElement(By.xpath("//button[normalize-space()='" + price + "']")).click();
        findElementAndClick(applyFilter_button);
        Thread.sleep(2000);
        LOGGER.info("Clicked Price and added Value");
    }

    public void clickShowResults() throws InterruptedException {
        findElementAndClick(show_results_button);
        Thread.sleep(2000);
    }


}
