package pages;

import config.ConfigPropertyReader;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static utils.Base.*;

public class LoginPage {
    private WebDriver driver;
    private String username = System.getProperty("username");

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    private By byLoginLogo = By.className("login_logo");
    private By byBotLogo = By.className("bot_column");
    private By byUsernameField = By.id("user-name");
    private By byPasswordField = By.id("password");
    private By byLoginButton = By.id("login-button");
    private By byErrorCrossButton = By.className("error-button");

    public void navigateToSauceDemo() {
        driver.get(ConfigPropertyReader.getInstance().getUrl());
        waitUntilElementIsClickable(byLoginButton);
        Assert.assertTrue(driver.findElement(byLoginLogo).isDisplayed());
        Assert.assertTrue(driver.findElement(byBotLogo).isDisplayed());
    }

    public void enterUsername(){
        driver.findElement(byUsernameField).sendKeys(System.getProperty("username"));
    }
    public void enterPassword(){
        driver.findElement(byPasswordField).sendKeys(ConfigPropertyReader.getInstance().getUserPassword());
    }
    public void enterCredentials(){
        enterUsername();
        enterPassword();
    }

    public void clickLoginButton(){
        driver.findElement(byLoginButton).click();
    }
    public void clickErrorCrossButton(){
        driver.findElement(byErrorCrossButton).click();
    }
}
