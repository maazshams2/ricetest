package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import static utils.Base.*;

public class InventoryPage {
    private WebDriver driver;

    public InventoryPage(WebDriver driver) {
        this.driver = driver;
    }

    private By byPageTitle = By.className("title");
    private By byShoppingCartIcon = By.className("shopping_cart_link");
    private By byShoppingCartBadge = By.className("shopping_cart_badge");
    private By byMenuButton = By.className("bm-burger-button");
    private By byMenu = By.className("bm-menu-wrap");
    private By byMenuCrossButton = By.className("bm-cross-button");
    private By byAllItems = By.id("inventory_sidebar_link");
    private By byAbout = By.id("about_sidebar_link");
    private By byLogout = By.id("logout_sidebar_link");
    private By byResetAppState = By.id("reset_sidebar_link");
    private By byFilterButton = By.className("active_option");
    private By byFilterOptions = By.className("product_sort_container");
    private By byInventoryItem = By.className("inventory_item");
    private By byAddRemoveToCartButton = By.className("btn_inventory");

    public void validateInventoryPage(){
        waitUntilElementIsClickable(byShoppingCartIcon);
        Assert.assertTrue(driver.findElement(byPageTitle).getText().equalsIgnoreCase("products"));
        Assert.assertTrue(driver.findElement(byShoppingCartIcon).isDisplayed());
    }

    public void clickShoppingCart(){
        driver.findElement(byShoppingCartIcon).click();
    }
    public int getCartItems(){
        return Integer.parseInt(driver.findElement(byShoppingCartBadge).getText());
    }

    public int getInventoryItemsCount(){
        return driver.findElements(byAddRemoveToCartButton).size();
    }
    public void clickAddToCart(int itemNumber){
        if (driver.findElements(byAddRemoveToCartButton).get(itemNumber).getText().equalsIgnoreCase("add to cart")){
            driver.findElements(byAddRemoveToCartButton).get(itemNumber).click();
            Assert.assertTrue(driver.findElements(byAddRemoveToCartButton).get(itemNumber).getText().equalsIgnoreCase( "remove"));
        }
    }
    public void clickRemoveToCart(int itemNumber){
        if (driver.findElements(byAddRemoveToCartButton).get(itemNumber).getText().equalsIgnoreCase("remove"))
            driver.findElements(byAddRemoveToCartButton).get(itemNumber).click();
    }

    // Menu
    public void clickMenuButton(){
        driver.findElement(byMenuButton).click();
        waitUntilElementIsVisible(byMenu);
        Assert.assertTrue(driver.findElement(byMenu).isDisplayed());
    }
    public void clickMenuCrossButton(){
        driver.findElement(byMenuCrossButton).click();
        waitUntilElementIsInvisible(byMenu);
        Assert.assertFalse(driver.findElement(byMenu).isDisplayed());
    }

    // Filter
    public void clickFilterButton(int filterIndex){
        driver.findElement(byFilterButton).click();
        waitUntilElementIsVisible(byFilterOptions);
        Select sel = new Select(driver.findElement(byFilterOptions));
        sel.selectByIndex(filterIndex);
    }

}
