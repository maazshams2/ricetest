@TestFrontEnd
Feature: Frontend tests of Sauce Demo

  Background: User navigates to Sauce Demo website
    Given User navigates to the sauce demo website

  Scenario: User logins with correct username and password
    When User enters correct credentials
    And User clicks on login
    Then User is successfully logged in

  Scenario: User adds items to cart
    And User logins successfully
    When User adds 3 random items to cart
    Then User clicks on cart icon
    And Cart has all items added to it

