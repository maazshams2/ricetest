# SauceDemo Automation
This project is for automating UI for saucedemo.com

# Installation Process
1. Clone the project
2. run "mvn clean install" command.

**Running Tests**
-
Execute tests via use of the mentioned command in terminal :

        mvn clean install -Dtest=runner.TestRunner -Dusername=standard_user

- _-Dusername_ variable used to enter username value

**Project Structure:**
-
1. Config contains ConfigPropertyReader that reads data from .property file.
2. pages contain multiple java classes that has implementation of stepDefinitions.
3. runner contains TestRunner.java that is used to run the project.
4. stepDefinitions contains multiple java classes that has steps defined in feature file.
5. stepDefinitions contains Hook.java that has browser initializer.
6. utils has Base.java that has helper methods, driver initializer.

**Scenarios Automated:**
-
1. User logins with correct username and password
2. User adds items to cart



